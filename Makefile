CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -Wextra -Isrc

clean:
	-rm src/*.o
	-rm test_linked_list

src/linked_list.o: src/linked_list.c
	$(CC) $(CFLAGS) -c src/linked_list.c -o src/linked_list.o

src/test_linked_list.o: src/test_linked_list.c
	$(CC) $(CFLAGS) -c src/test_linked_list.c -o src/test_linked_list.o

test_linked_list: src/linked_list.o src/test_linked_list.o
	$(CC) $^ -o test_linked_list;

