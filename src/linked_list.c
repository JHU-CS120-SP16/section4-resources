#include <stddef.h>

#include "linked_list.h"

// Constructors/Destructors

struct linked_list ll_init() {
  struct linked_list list = { NULL };
  return list;
}

void ll_free(struct linked_list list) {
  return;
}

// Basic Information

bool ll_empty(struct linked_list list) {
  return true;
}

int ll_size(struct linked_list list) {
  return 0;
}

void ll_display(struct linked_list list) {
  return;
}

// Accessors

struct ll_node* ll_head(struct linked_list list) {
  return NULL;
}

struct ll_node* ll_tail(struct linked_list list) {
  return NULL;
}

struct ll_node* ll_at(struct linked_list list, int position) {
  return NULL;
}

// Mutators

struct ll_node* ll_insert(struct linked_list list, int position, int value) {
  return NULL;
}

void ll_delete(struct linked_list list, int position) {
  return;
}

// Transformers

struct linked_list ll_filter_lt(struct linked_list list, int bound) {
  return ll_init();
}

int ll_sum(struct linked_list list) {
  return 0;
}
