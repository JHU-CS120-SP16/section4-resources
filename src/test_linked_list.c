#include <stdio.h>

#include "linked_list.h"

int main() {
  struct linked_list list = ll_init();

  ll_insert(list, 0, 0);
  ll_insert(list, 1, 1);
  ll_insert(list, 2, 1);
  ll_insert(list, 3, 2);

  ll_insert(list, -1, 3);
  ll_insert(list, -1, 5);

  ll_insert(list, 2, 8);
  ll_insert(list, 5, 13);

  ll_display(list);

  ll_delete(list, 4);
  ll_delete(list, 17);

  ll_display(list);

  printf("Size: %d; Is Empty: %d\n", ll_size(list), ll_empty(list));

  if (!ll_empty(list)) {
    printf("First + Last = %d", ll_head(list)->payload + ll_tail(list)->payload);
  }
  printf("Total: %d\n", ll_sum(list));

  struct linked_list filtered = ll_filter_lt(list, 6);

  printf("Filtered list is: ");
  printf("\n");

  ll_display(filtered);

  ll_free(list);
  ll_free(filtered);

  return 0;
}
