#ifndef LINKED_LIST
#define LINKED_LIST

#include <stdbool.h>

// Struct node and header definitions.

struct ll_node {
  int payload;
  struct ll_node *next;
};

struct linked_list {
  struct list_node *head;
};

// Constructors/Destructors

struct linked_list ll_init();
void ll_free(struct linked_list);

// Basic information.

bool ll_empty(struct linked_list);
int  ll_size(struct linked_list);

void ll_display(struct linked_list);

// Accessors

struct ll_node* ll_head(struct linked_list);    // Return pointer to first node.
struct ll_node* ll_tail(struct linked_list);    // Return pointer to last node.
struct ll_node* ll_at(struct linked_list, int); // Return pointer to node at index.

// Mutators

struct ll_node* ll_insert(struct linked_list, int, int); // Insert at position, value. Return pointer to new node.
void            ll_delete(struct linked_list, int);      // Delete at position.

// Transformers

struct linked_list ll_filter_lt(struct linked_list, int); // Filter out elements less than given value, create new list.
int ll_sum(struct linked_list); // Compute the sum of all elements in the list.

#endif
